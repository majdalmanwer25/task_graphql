class UsersModel {
  Data? data;

  UsersModel({this.data});

  UsersModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  List<Users>? users;

  Data({this.users});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['users'] != null) {
      users = <Users>[];
      json['users'].forEach((v) {
        users!.add(new Users.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.users != null) {
      data['users'] = this.users!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Users {
  String? id;
  String? userId;
  String? firstName;
  String? backgroundColor;
  String? blockNumber;
  String? instagram;
  List<String>? tags;
  int? followedUsersCount;
  int? followingUsersCount;
  List<String>? followedUsers;
  List<String>? followers;

  Users(
      {this.id,
      this.userId,
      this.firstName,
      this.backgroundColor,
      this.blockNumber,
      this.instagram,
      this.tags,
      this.followedUsersCount,
      this.followingUsersCount,
      this.followedUsers,
      this.followers});

  Users.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    firstName = json['firstName'];
    backgroundColor = json['backgroundColor'];
    blockNumber = json['blockNumber'];
    instagram = json['instagram'];
    tags = json['tags'].cast<String>();
    followedUsersCount = json['followedUsersCount'];
    followingUsersCount = json['followingUsersCount'];
    followedUsers = json['followedUsers'].cast<String>();
    followers = json['followers'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['firstName'] = this.firstName;
    data['backgroundColor'] = this.backgroundColor;
    data['blockNumber'] = this.blockNumber;
    data['instagram'] = this.instagram;
    data['tags'] = this.tags;
    data['followedUsersCount'] = this.followedUsersCount;
    data['followingUsersCount'] = this.followingUsersCount;
    data['followedUsers'] = this.followedUsers;
    data['followers'] = this.followers;
    return data;
  }
}
