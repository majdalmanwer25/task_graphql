import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:graphql_task/constant/app_quary.dart';
import 'package:graphql_task/model/graph.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

final UsersModel userModel = UsersModel();

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Query(
        options: QueryOptions(
          document: gql(GraphQLAPIQUERY),
        ),
        builder: (QueryResult result, {fetchMore, refetch}) {
          if (result.hasException) {
            return Text(result.exception.toString());
          }
          if (result.isLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          final usersList = result.data?["users"];
          print(usersList);
          return ListView.builder(
              itemCount: usersList.length,
              itemBuilder: (_, index) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Container(
                        child: Text(usersList[index]["firstName"]),
                      ),
                    ),

                    Container(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(usersList[index]["instagram"])),
                    Container(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(usersList[index]["tags"].toString())),
                    // Container(
                    //   padding: EdgeInsets.all(8),
                    //   width: 150,
                    //   height: 150,
                    //   child: Image.network(
                    //     usersList[index]["instagram"],
                    //   ),
                    // ),
                    Container(
                      child: Text(usersList[index]["blockNumber"]),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.all(8),
                          child: Text(usersList[index]["followedUsersCount"]
                              .toString()),
                        ),
                        Container(
                            padding: EdgeInsets.all(8),
                            child: Text(usersList[index]["followingUsersCount"]
                                .toString())),
                        Container(
                            padding: EdgeInsets.all(8),
                            child: Text(
                                usersList[index]["followedUsers"].toString())),
                      ],
                    )
                  ],
                );
              });
        },
      ),
    );
  }
}
