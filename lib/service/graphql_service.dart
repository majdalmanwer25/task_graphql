import 'package:flutter/cupertino.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final HttpLink httpLink = HttpLink(
    "https://api.thegraph.com/subgraphs/name/dalhoum-ahmed/aurora-test-user-post-3");

ValueNotifier<GraphQLClient> client = ValueNotifier(GraphQLClient(
  link: httpLink,
  cache: GraphQLCache(store: InMemoryStore()),
));
